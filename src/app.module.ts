import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrdersModule } from './orders/orders.module';
import config from '../config';

@Module({
  imports: [MongooseModule.forRoot(config.MONGO_URI), OrdersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
