import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  HttpService,
} from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order } from './schemas/order.schema';
import config from '../../config';

@Controller('orders')
export class OrdersController {
  constructor(
    private readonly ordersService: OrdersService,
    private readonly httpService: HttpService,
  ) {}

  @Post()
  async create(@Body() createOrderDto: CreateOrderDto) {
    const response = await this.httpService
      .post(`${config.PAYMENTS_URI}/payments`, {
        token: 'secret_token',
      })
      .toPromise();

    if (response.data.status !== 'confirmed') {
      createOrderDto.status = 'cancelled';
      return this.ordersService.create(createOrderDto);
    }

    createOrderDto.status = 'created';
    const createdOrder = await this.ordersService.create(createOrderDto);

    createOrderDto.status = 'confirmed';

    setTimeout(async () => {
      createOrderDto.status = 'delivered';
      await this.ordersService.update(createdOrder._id, createOrderDto);
    }, 10000);

    return this.ordersService.update(createdOrder._id, createOrderDto);
  }

  @Get()
  async findAll(): Promise<Order[]> {
    return await this.ordersService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Order> {
    return await this.ordersService.findOne(id);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() createOrderDto: CreateOrderDto,
  ) {
    return await this.ordersService.update(id, createOrderDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.ordersService.remove(id);
  }
}
