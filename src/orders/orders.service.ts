import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order, OrderDocument } from './schemas/order.schema';

@Injectable()
export class OrdersService {
  constructor(
    @InjectModel('Order')
    private readonly orderModel: Model<OrderDocument>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const order = new this.orderModel(createOrderDto);
    return await order.save();
  }

  async findAll(): Promise<Order[]> {
    return await this.orderModel.find();
  }

  async findOne(id: string): Promise<Order> {
    return await this.orderModel.findById(id);
  }

  update(id: string, createOrderDto: CreateOrderDto) {
    return this.orderModel.findByIdAndUpdate(id, createOrderDto, {
      new: true,
      useFindAndModify: false,
    });
  }

  remove(id: string) {
    return this.orderModel.findByIdAndRemove(id, { useFindAndModify: false });
  }
}
