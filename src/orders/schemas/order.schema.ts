import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OrderDocument = Order & Document;

@Schema({ timestamps: true })
export class Order {
  @Prop()
  token: string;

  @Prop()
  status: string;

  @Prop()
  details: string;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
