import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { OrdersModule } from '../src/orders/orders.module';
import { CreateOrderDto } from 'src/orders/dto/create-order.dto';
import * as mongoose from 'mongoose';
import config from '../config';

let app: INestApplication;

beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule, OrdersModule],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();
  await mongoose.connect(config.MONGO_URI);
});

afterAll(async (done) => {
  await app.close();
  await mongoose.disconnect(done);
});

describe('AppController (e2e)', () => {
  it('should return Hello World', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
});

describe('OrderController (e2e)', () => {
  it('should create an order', async () => {
    const newOrder: CreateOrderDto = {
      status: 'created',
      details: 'ayam',
    };
    return await request(app.getHttpServer())
      .post('/orders')
      .set('Accept', 'application/json')
      .send(newOrder)
      .expect(HttpStatus.CREATED)
      .expect(({ body }) => {
        expect(body).toHaveProperty('_id');
        expect(body).toHaveProperty('status');
      });
  });

  it('should return all the orders', async () => {
    return await request(app.getHttpServer())
      .get('/orders')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body[0]).toHaveProperty('_id');
        expect(body[0]).toHaveProperty('status');
      });
  });

  it('should return one order from specific id', async () => {
    const id = await (await request(app.getHttpServer()).get('/orders')).body[0]
      ._id;

    return await request(app.getHttpServer())
      .get(`/orders/${id}`)
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body._id).toEqual(id);
      });
  });

  it('should update order status from specific id', async () => {
    const newOrder: CreateOrderDto = {
      status: 'confirmed',
      details: 'ikan',
    };
    const id = await (
      await request(app.getHttpServer()).get('/orders')
    ).body.find(({ status }) => status === 'created')._id;

    return await request(app.getHttpServer())
      .put(`/orders/${id}`)
      .send(newOrder)
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body._id).toEqual(id);
      });
  });

  it('should delete order from specific id', async () => {
    const id = await (await request(app.getHttpServer()).get('/orders')).body[0]
      ._id;

    return await request(app.getHttpServer())
      .delete(`/orders/${id}`)
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body._id).toEqual(id);
      });
  });
});
