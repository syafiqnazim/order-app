import * as dotenv from 'dotenv';
dotenv.config();

export default {
  PORT: process.env.PORT,
  MONGO_URI: process.env.MONGO_URI,
  PAYMENTS_URI: process.env.PAYMENTS_URI || 'http://localhost:4000/api',
};
